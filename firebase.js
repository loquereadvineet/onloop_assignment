require('dotenv').config();
const { initializeApp } = require('firebase/app');
const { getFirestore, connectFirestoreEmulator } = require('firebase/firestore');
const { config } = require('./firebase_config');

const app = initializeApp(config);
const db = getFirestore(app);

if (process.env.mode === 'DEV') {
  connectFirestoreEmulator(db, 'localhost', 8888);
}

module.exports = {
  db,
};
