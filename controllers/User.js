const utils = require('../utils/writer');
const User = require('../service/UserService');

module.exports.createUser = function createUser(req, res, next) {
  const body = req.swagger.params.body.value;
  User.createUser(body)
    .then((response) => {
      utils.writeJson(res, response[0], response[1]);
    })
    .catch((response) => {
      utils.writeJson(res, response[0], response[1]);
    });
};

module.exports.deleteUser = function deleteUser(req, res, next) {
  const email = req.swagger.params.email.value;
  User.deleteUser(email)
    .then((response) => {
      utils.writeJson(res, response[0], response[1]);
    })
    .catch((response) => {
      console.log(response);
      utils.writeJson(res, response[0], response[1]);
    });
};

module.exports.getUserByEmail = function getUserByEmail(req, res, next) {
  const email = req.swagger.params.email.value;
  User.getUserByEmail(email)
    .then((response) => {
      utils.writeJson(res, response[0], response[1]);
    })
    .catch((response) => {
      utils.writeJson(res, response[0], response[1]);
    });
};

module.exports.updateUser = function updateUser(req, res, next) {
  const email = req.swagger.params.email.value;
  const body = req.swagger.params.body.value;
  User.updateUser(email, body)
    .then((response) => {
      utils.writeJson(res, response[0], response[1]);
    })
    .catch((response) => {
      utils.writeJson(res, response[0], response[1]);
    });
};
