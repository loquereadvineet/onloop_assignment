const utils = require('../utils/writer');
const LinkPreview = require('../service/LinkPreviewService');

module.exports.fetchLinkPreview = function fetchLinkPreview(req, res, next) {
  const body = req.swagger.params.body.value;
  LinkPreview.fetchLinkPreview(body)
    .then((response) => {
      utils.writeJson(res, response[0], response[1]);
    })
    .catch((response) => {
      utils.writeJson(res, response[0], response[1]);
    });
};
