const {
  collection, query, where, updateDoc, deleteDoc, addDoc, getDocs, setDoc,
} = require('firebase/firestore');
const { db } = require('../firebase');
const { validateEmail, validatePhone, validateName } = require('../utils/validators');

/**
 * Create user
 * The function is responsible to add a user in user collection in firebase,
 * */
exports.createUser = function (body) {
  return new Promise(async (resolve, reject) => {
    // validate all input data is present in payload
    if (!body.phone || !body.email || !body.name) {
      reject(['Incomplete Payload', 400]);
      return;
    }

    // validate data standards
    if (!validateEmail(body.email)) {
      reject(['Incorrect Email Address', 400]);
      return;
    }

    if (!validatePhone(body.phone)) {
      reject(['Incorrect Phone Number', 400]);
      return;
    }

    if (!validateName(body.name)) {
      reject(['Incorrect Name', 400]);
      return;
    }

    // user entry logic
    const q = query(collection(db, 'users'), where('email', '==', body.email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size === 0) {
      try {
        addDoc(collection(db, 'users'), {
          phone: body.phone,
          email: body.email,
          name: body.name,
        }).then((res) => {
          resolve([`User created with Id : ${res.id}`, 201]);
        });
      } catch (e) {
        console.error('Error adding document: ', e);
        reject([e, 500]);
      }
    } else {
      reject(['This email is already registered', 400]);
    }
  });
};

/**
 * Delete user
 *
 * email of user that needs to be deleted
 * */
exports.deleteUser = function (email) {
  return new Promise(async (resolve, reject) => {
    // validate all input data is present in payload
    if (!email || email === '') {
      reject(['Please enter email address', 400]);
      return;
    }

    // validate data standards

    if (!validateEmail(email)) {
      reject(['Incorrect Email Address', 400]);
      return;
    }

    // user delete logic
    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((doc) => {
          addDoc(collection(db, 'deleted_users'), {
            userid: doc.id,
            data: JSON.stringify(doc.data()),
          }).then(() => {
            setDoc(doc.ref, {});
            //https://firebase.google.com/docs/firestore/manage-data/delete-data#collections 
            //subcollections wont be deleted by deleteDoc, setDoc doesnt apply on subcollectiosn, it is suggested to do it manually
            //however, this can be accomplished by writing another cloud function / api which uses firebase-admin to do this
            deleteDoc(doc.ref);
            resolve(['Delete Successful', 202]);
          });
        });
      } catch (e) {
        console.error('Error deleting document: ', e);
        reject([e, 500]);
      }
    } else {
      reject(['Nothing to delete', 400]);
    }
  });
};

/**
 * Get user by email
 *
 *
 * email : of the user that needs to be fetched.
 * returns User
 * */
exports.getUserByEmail = function (email) {
  return new Promise(async (resolve, reject) => {
    // validate all input data is present in payload
    if (!email) {
      reject(['Please enter email address', 400]);
      return;
    }

    // validate data standards

    if (!validateEmail(email)) {
      reject(['Incorrect Email Address', 400]);
      return;
    }

    const user = [];

    // user update logic
    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size === 0) {
      reject(['No user found', 400]);
      return;
    }
    querySnapshot.forEach((doc) => {
      user.push(doc.data());
    });
    resolve([user, 200]);
  });
};

/**
 * Update user
 *
 * username of the object  that need to be updated
 * body which needs to be pushed in
 *
 * */
exports.updateUser = function (email, body) {
  return new Promise(async (resolve, reject) => {
    // validate all input data is present in payload
    if (!email) {
      reject(['Please enter email address', 400]);
      return;
    }

    if (!body.phone || !body.email || !body.name) {
      reject(['Incomplete Payload', 400]);
      return;
    }

    // validate data standards
    if (!validateEmail(email)) {
      reject(['Incorrect Email Address', 400]);
      return;
    }

    if (!validateEmail(body.email)) {
      reject(['Incorrect Email Address', 400]);
      return;
    }

    if (!validatePhone(body.phone)) {
      reject(['Incorrect Phone Number', 400]);
      return;
    }

    if (!validateName(body.name)) {
      reject(['Incorrect Name', 400]);
      return;
    }

    // user update logic
    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((doc) => {
          updateDoc(doc.ref, {
            email: body.email,
            name: body.name,
            phone: body.phone,
          }).then((res) => {
            resolve(['Update Successful', 202]);
          });
        });
      } catch (e) {
        console.error('Error updating document: ', e);
        reject([e, 500]);
      }
    } else {
      reject(['No user to update', 400]);
    }
  });
};
