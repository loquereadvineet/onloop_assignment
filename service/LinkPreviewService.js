require('dotenv').config();
const {
  doc, addDoc, getDoc, serverTimestamp, collection,
} = require('firebase/firestore');
const axios = require('axios');
const { db } = require('../firebase');
const { validateUrl } = require('../utils/validators');

// convert linkpreview error codes to messages
const errorCodeToMessage = function errorCodeToMessage(code) {
  const codelist = {
    400: 'Generic error',
    401: 'Cannot verify API access key',
    403: 'Invalid or blank API access key',
    423: 'Forbidden by robots.txt - the requested website does not allow us to access this page',
    425: 'Invalid response status code from url',
    426: 'Too many requests per second on a single domain',
    429: 'Too many requests / rate limit exceeded',
  };

  return codelist[code];
};

/**
 * Fetch Data From Link Preview
 * This is to fetch data from linkpreview
 *
 * body LinkPreview
 * no response value expected for this operation
 * */
exports.fetchLinkPreview = function (body) {
  return new Promise(async (resolve, reject) => {
    const key = process.env.linkPreviewKey;

    // validate all input data is present in payload
    if (!body.url || !body.user_id) {
      reject(['Incomplete Payload', 400]);
      return;
    }

    // validate data standards
    if (!validateUrl(body.url)) {
      reject(['Incorrect Url', 400]);
      return;
    }

    const docRef = doc(db, 'users', body.user_id);
    const docSnap = await getDoc(docRef);

    // check if user id is a valid user id from firestore
    if (docSnap.exists()) {
      // hit the linkpreview api
      axios.post(
        'https://api.linkpreview.net',
        {
          q: body.url,
          key,
        },
      ).then(async (resp) => {
        // Update the user collection and the data to firestore
        const docRef = await addDoc(
          collection(db, 'users', body.user_id, 'learn_content'),
          {
            created: serverTimestamp(),
            description: resp.data.description,
            image: resp.data.image,
            status: 'unread',
            title: resp.data.title,
            url: resp.data.url,
          },
        );

        // UPDATE the collection with tags collection inside it
        if (body.tags) {
          const tags = body.tags.split(',');
          tags.forEach(async (element) => {
            await addDoc(
              collection(db, 'users', body.user_id, 'learn_content', docRef.id, 'tags'),
              {
                name: element,
                is_custom: 'false',
              },
            );
          });
        }
        resolve(['LinkPreview Search Successfull', 202]);
      }).catch((error) => { // catch errors from LP API
        const code = error.response.status;
        const message = errorCodeToMessage(code);
        console.log(message);
        reject([message, code]);
      });
    } else {
      reject(['Invalid User ID', 400]);
    }
  });
};
