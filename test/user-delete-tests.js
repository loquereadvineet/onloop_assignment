const { expect } = require('chai');
const {
  collection, query, where, deleteDoc, addDoc, getDocs, setDoc,
} = require('firebase/firestore');
const { db } = require('../firebase');
const User = require('../service/UserService');

describe('Test the deletion of Users', () => {
  before(() => new Promise(async (resolve) => {
    const body = {
      phone: '9983120888',
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
    };
    const q = query(collection(db, 'users'), where('email', '==', body.email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size === 0) {
      try {
        addDoc(collection(db, 'users'), {
          phone: body.phone,
          email: body.email,
          name: body.name,
        }).then(() => {
          console.log(['Demo user created for updation']);
          resolve();
        });
      } catch (e) {
        console.error('Error adding document: ', e);
        reject();
      }
    } else {
      console.log(['Demo user already present in db']);
      reject();
    }
  }));

  after(() => new Promise(async (resolve) => {
    const email = 'v2n.sh@outlook.com';

    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((doc) => {
          setDoc(doc.ref, {});
          deleteDoc(doc.ref);
          console.log(['Database is clear of test users']);
          resolve();
        });
      } catch (e) {
        console.error('Error deleting document: ', e);
        resolve();
      }
    } else {
      console.log(['Database is clear of test users']);
      resolve();
    }
  }));

  it('Throws error on missing email', async () => {
    const email = '';
    await User.deleteUser(email, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on missing email, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Please enter email address');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid email', async () => {
    const email = 'vn.sh@outloo@k.com';
    await User.deleteUser(email, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on invalid email, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Email Address');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on unregistered user', async () => {
    const email = 'v2n.sh@outlook.com';
    await User.deleteUser(email, {}, () => { }).then(async (result) => {
      console.log(result);
      console.log('Expected the function to not throw an error, test failed');
      throw new Error();
    }).catch((error) => {
      expect(error[0]).equal('Nothing to delete');
      expect(error[1]).equal(400);
    });
  });

  it('Deletes a user from database', async () => {
    const email = 'vn.sh@outlook.com';
    await User.deleteUser(email, {}, () => { }).then(async (result) => {
      expect(result[0]).equal('Delete Successful');
      expect(result[1]).equal(202);
    }).then(async () => {
      const q = query(collection(db, 'users'), where('email', '==', 'vn.sh@outlook.com'));
      const querySnapshot = await getDocs(q);
      if (querySnapshot.size > 0) {
        console.log('Expected the function to delete user, test failed');
        throw new Error();
      }
    }).catch((error) => {
      console.log(error);
      console.log('Expected the function to not throw an error, test failed');
      throw new Error();
    });
  });
});
