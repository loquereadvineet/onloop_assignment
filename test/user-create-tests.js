const { expect } = require('chai');
const {
  collection, query, where, doc, getDoc, deleteDoc, getDocs, setDoc,
} = require('firebase/firestore');
const { db } = require('../firebase');
const User = require('../service/UserService');

describe('Test the Creation of Users', () => {
  before(() => new Promise(async (resolve) => {
    const q = query(collection(db, 'users'), where('email', '==', 'vn.sh@outlook.com'));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((doc) => {
          setDoc(doc.ref, {});
          deleteDoc(doc.ref);
          resolve();
        });
      } catch (e) {
        console.error('Error deleting document: ', e);
        resolve();
      }
    } else {
      console.log(['Database is clear of test users']);
      resolve();
    }
  }));

  after(() => new Promise(async (resolve) => {
    const q = query(collection(db, 'users'), where('email', '==', 'vn.sh@outlook.com'));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((document) => {
          setDoc(document.ref, {});
          deleteDoc(document.ref);
          console.log(['Database is clear of test users']);
          resolve();
        });
      } catch (e) {
        console.error('Error deleting document: ', e);
        resolve();
      }
    } else {
      console.log(['Database is clear of test users']);
      resolve();
    }
  }));

  it('Throws error on empty body', async () => {
    const body = {};
    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing email in body (INCOMPLETE PAYLOAD)', async () => {
    const body = {
      phone: '9983108388',
      name: 'Vineet',
    };
    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on missing email, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing phone in body (INCOMPLETE PAYLOAD)', async () => {
    const body = {
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
    };

    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on missing phone, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing name in body (INCOMPLETE PAYLOAD)', async () => {
    const body = {
      phone: '9983108388',
      email: 'vn.sh@outlook.com',
    };

    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on missing name, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid email in body', async () => {
    const body = {
      phone: '9983108388',
      name: 'Vineet',
      email: 'vn.sh@outloo@k.com',
    };

    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on invalid email, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Email Address');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid phone in body', async () => {
    const body = {
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
      phone: '9983dadad8388',
    };

    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on invalid phone, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Phone Number');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid name in body', async () => {
    const body = {
      phone: '9983108388',
      email: 'vn.sh@outlook.com',
      name: '31Vhad@',
    };
    await User.createUser(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on invalid name, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Name');
      expect(result[1]).equal(400);
    });
  });

  it('Inserts a user with valid details into db', async () => {
    const body = {
      phone: '9983120888',
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
    };
    await User.createUser(body, {}, () => { }).then(async (res) => {
      const userId = res[0].split(':')[1].trim();
      const docRef = doc(db, 'users', userId);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        const docPhone = docSnap.data().phone;
        const docName = docSnap.data().name;
        const docEmail = docSnap.data().email;
        expect(docPhone).equal(body.phone);
        expect(docName).equal(body.name);
        expect(docEmail).equal(body.email);
      } else {
        console.log('No user found, create user failure.');
        throw new Error();
      }
    }).catch((error) => {
      console.log(error);
      console.log('Expected the function to not throw an error, test failed');
      throw new Error();
    });
  });
});
