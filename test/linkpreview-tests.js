const { expect } = require('chai');
const {
  collection, query, where, deleteDoc, addDoc, getDocs, setDoc,
} = require('firebase/firestore');
const { db } = require('../firebase');
const Linkpreview = require('../service/LinkPreviewService');

describe('Test the Linkpreview API', () => {
  let userId = ''; // we will store userId for testing the LP API

  before(() => new Promise(async (resolve) => {
    const body = {
      phone: '9983120888',
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
    };
    const q = query(collection(db, 'users'), where('email', '==', body.email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size === 0) {
      try {
        addDoc(collection(db, 'users'), {
          phone: body.phone,
          email: body.email,
          name: body.name,
        }).then((res) => {
          console.log(['Demo user created for updation']);
          userId = res.id;
          resolve();
        });
      } catch (e) {
        console.error('Error adding document: ', e);
        reject();
      }
    } else {
      console.log(['Demo user already present in db']);
      reject();
    }
  }));

  after(() => new Promise(async (resolve) => {
    const email = 'vn.sh@outlook.com';

    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((doc) => {
          setDoc(doc.ref, {});
          deleteDoc(doc.ref);
          console.log(['Database is clear of test users']);
          resolve();
        });
      } catch (e) {
        console.error('Error deleting document: ', e);
        resolve();
      }
    } else {
      console.log(['Database is clear of test users']);
      resolve();
    }
  }));

  it('Throws error on missing url', async () => {
    const body = {
      user_id: '123',
    };
    await Linkpreview.fetchLinkPreview(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing userId', async () => {
    const body = {
      url: 'www.google.com',
    };
    await Linkpreview.fetchLinkPreview(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid url', async () => {
    const body = {
      url: 'wwwgoogleom',
      user_id: '123',
    };
    await Linkpreview.fetchLinkPreview(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on invalid url, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Url');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on unregistered user', async () => {
    const body = {
      url: 'www.google.com',
      user_id: '123',
    };
    await Linkpreview.fetchLinkPreview(body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on invalid user, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Invalid User ID');
      expect(result[1]).equal(400);
    });
  });

  it('Inserts linkpreview data into firestore', async () => {
    const body = {
      url: 'www.google.com',
      user_id: userId,
      tags: 'vineet, google, search, test',
    };
    await Linkpreview.fetchLinkPreview(body, {}, () => { }).then((res) => {
      expect(res[0]).equal('LinkPreview Search Successfull');
      expect(res[1]).equal(202);
    }).catch(() => {
      console.log('Expected the function to not throw an error, test failed');
      throw new Error();
    });
  }).timeout(10000);
});
