const { expect } = require('chai');
const {
  collection, query, where, deleteDoc, addDoc, getDocs, setDoc,
} = require('firebase/firestore');
const { reject } = require('firebase-tools/lib/utils');
const { db } = require('../firebase');
const User = require('../service/UserService');

describe('Test the Updation of Users', () => {
  before(() => new Promise(async (resolve) => {
    const body = {
      phone: '9983120888',
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
    };
    const q = query(collection(db, 'users'), where('email', '==', body.email));
    const querySnapshot = await getDocs(q);
    if (querySnapshot.size === 0) {
      try {
        addDoc(collection(db, 'users'), {
          phone: body.phone,
          email: body.email,
          name: body.name,
        }).then(() => {
          console.log(['Demo user created for updation']);
          resolve();
        });
      } catch (e) {
        console.error('Error adding document: ', e);
        reject();
      }
    } else {
      console.log(['Demo user already present in db']);
      reject();
    }
  }));

  after(() => new Promise(async (resolve) => {
    const email = 'v2n.sh@outlook.com';

    const q = query(collection(db, 'users'), where('email', '==', email));
    const querySnapshot = await getDocs(q);

    if (querySnapshot.size > 0) {
      try {
        querySnapshot.forEach((doc) => {
          setDoc(doc.ref, {});
          deleteDoc(doc.ref);
          console.log(['Database is clear of test users']);
          resolve();
        });
      } catch (e) {
        console.error('Error deleting document: ', e);
        resolve();
      }
    } else {
      console.log(['Database is clear of test users']);
      resolve();
    }
  }));

  it('Throws error on empty body', async () => {
    const body = {};
    const email = 'vn.sh@outlook.com';
    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing email in body (INCOMPLETE PAYLOAD)', async () => {
    const body = {
      phone: '9983108388',
      name: 'Vineet',
    };
    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing phone in body (INCOMPLETE PAYLOAD)', async () => {
    const body = {
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
    };
    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing name in body (INCOMPLETE PAYLOAD)', async () => {
    const body = {
      phone: '9983108388',
      email: 'vn.sh@outlook.com',
    };
    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incomplete Payload');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on missing email which is to be updated', async () => {
    const body = {
      phone: '9983108388',
      email: 'vn.sh@outlook.com',
      name: '31Vhad@',
    };

    const email = '';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Please enter email address');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid email which is to be updated', async () => {
    const body = {
      phone: '9983108388',
      name: 'Vineet',
      email: 'vn.sh@outlook.com',
    };
    const email = 'vn.sh@outloo@k.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Email Address');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid email in body', async () => {
    const body = {
      phone: '9983108388',
      name: 'Vineet',
      email: 'vn.sh@outloo@k.com',
    };
    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Email Address');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid phone in body', async () => {
    const body = {
      email: 'vn.sh@outlook.com',
      name: 'Vineet',
      phone: '9983dadad8388',
    };

    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Phone Number');
      expect(result[1]).equal(400);
    });
  });

  it('Throws error on invalid name in body', async () => {
    const body = {
      phone: '9983108388',
      email: 'vn.sh@outlook.com',
      name: '31Vhad@',
    };

    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(() => {
      console.log('Expected the function to throw an error on empty body, test failed');
      throw new Error();
    }).catch((result) => {
      expect(result[0]).equal('Incorrect Name');
      expect(result[1]).equal(400);
    });
  });

  it('Updates the user details into the database', async () => {
    const body = {
      phone: '9116720020',
      email: 'v2n.sh@outlook.com',
      name: 'Vineet',
    };
    const email = 'vn.sh@outlook.com';

    await User.updateUser(email, body, {}, () => { }).then(async (res) => {
      expect(res[0]).equal('Update Successful');
      expect(res[1]).equal(202);
      const checkEmail = body.email;
      const q = query(collection(db, 'users'), where('email', '==', checkEmail));
      const querySnapshot = await getDocs(q);
      if (querySnapshot.size === 0) {
        console.log('Expected the function to update the user, no user found, test failed');
        throw new Error();
      }
      querySnapshot.forEach((doc) => {
        expect(doc.data().phone).equal(body.phone);
        expect(doc.data().email).equal(body.email);
        expect(doc.data().name).equal(body.name);
      });
    }).catch((error) => {
      console.log(error);
      console.log('Expected the function to not throw an error, test failed');
      throw new Error();
    });
  });
});
