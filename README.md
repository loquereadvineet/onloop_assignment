
  

# Onloop Assignment Submission

## Overview

  

This is a nodejs application which serves API required for onloop assignment. The req document can be found on notion.

  

The API definition can be found in /api/swagger.yaml

  

**To test locally ,**

(1) Modify .env to point to "DEV" (by default in repo)

(2) If .env is pointing to "DEV", the application will target local firebase emulator, if not, it will target the firebase service online.

(2) Add linkpreview API key in .env file

(3) Setup and config firebase emulator , run via firebase emulators:start. Only firestore emulator is needed.

(4) The app is looking for firestore emulator on port 8888 with UI available on port 8889.

(5) Update firebase config in firebase_config.js in the following format :

  

exports.config = {

apiKey: '',

authDomain: '',

projectId: '',

storageBucket: '',

messagingSenderId: '',

appId: '',

};

  

(6)

  

Attached LOOM video of the project running can be found here :
https://www.loom.com/share/7808b697e881426a937299d0518cbede
  

Sample requests can be found in a postman collection here : https://www.postman.com/collections/62c15320c538322276c9

  

How to import a postman collection ?

Here :

http://makeseleniumeasy.com/2019/04/06/api-testing-tutorial-part-30-sharing-importing-collections-as-a-link-in-postman/

  

### Running the server

To run the server, run:

  

  

```

  

npm run dev

  

```

To check the api documentation locally , run the project and open:

  

  

```

  

http://localhost:8080/docs/

  

```

  

To run the tests, run:

  

  

```

  

npm run test

  

```

  

  

**TO DEPLOY**

  

1. User a production manager like https://pm2.keymetrics.io/ to deploy and monitor app

2. Suggestion to integrate a logging service to monitor application logs`